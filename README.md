"# My project's README" 
This game is intended to be ported into Google DayDream device for a better immersive experience, and ported in time for my portfolio class

The main game still has some issues to finish;
- The animations for the robot need revision
- The pursuit system need revisions; currently the robot movement is a little erractic, and does not stop after reaching the player;
- I wanna replace the current texture for the warning sign to a semi-transparent GUI element that fades according to the player's position;
- the level will be controlled by the daydream controlers as an ancient level maze.


Logic for the animations:
The animations states the robot should follow are:
1. Standup
2. Idle until detection
3. walking when detected
4. Running if player escaping;
5. Back to idle if player escapes



GUI Ideas:
- The warning sign should be places in the screen instead of the wall
- Make the texture appear slowly according to the player coming towards the wall.
- First pass will be a simple text showing up at the screen to warn the player


Improvement Log and Bug List
- Running animation is not looping as supposed.  The animation clip was checked for the loop settings but no changes;
- The game over cycle does not stops the controller from the player.
- Time scale is not being applied at the end of the game
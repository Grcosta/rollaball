﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PursuitPlayer : MonoBehaviour
{
    NavMeshAgent agent;
    public Transform playerTransform;
    public Animator animRobot;
    public float robotWalkSpeed = 2f;
    public float robotRunSpeed = 3.5f;
    public float distanceToDetect = 10f;


    //Animation States
    private bool isWalking = false;
    private bool isRunning = false;
    private bool isIdle = false;
    private bool isDying = false;


    // Use this for initialization
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();

        isIdle = true;
        animRobot.SetBool("isIdle", true);

        //[TODO] Move to better function

    }

    // Update is called once per frame
    void Update()
    {
        /*Animation settings:
        - Standup at start
        - Idle state until ball gets closer than 5
        - Walk towards ball (speed at 2)
        - Run towards ball if distance between 5 and 10
        - Return to idle if distance > 10    */


        FindPlayer();


    }

    void FindPlayer()
    {
        NavMeshHit target = new NavMeshHit();
        agent.Raycast(playerTransform.position, out target);

        if (target.distance < 10)
        {
            isIdle = false;
            isWalking = true;
            isRunning = false;
            animRobot.SetBool("isIdle", false);
            animRobot.SetBool("isWalking", true);
            animRobot.SetBool("isRunning", false);
            WalkToPlayer();
        }

        else if(target.distance > 10 && target.distance < 15)
        {
            isIdle = false;
            isWalking = false;
            isRunning = true;
            animRobot.SetBool("isIdle", false);
            animRobot.SetBool("isWalking", false);
            animRobot.SetBool("isRunning", true);
            RuntoPlayer();
        }

        else
        {
            isIdle = true;
            isWalking = false;
            isRunning = false;
            animRobot.SetBool("isIdle", true);
            animRobot.SetBool("isWalking", false);
            animRobot.SetBool("isRunning", false);
            agent.SetDestination(this.transform.position);
        }

    }

    void WalkToPlayer()
    {
        if (isWalking)
        {
            agent.speed = robotWalkSpeed;
            agent.SetDestination(playerTransform.position);
        }
    }

    void RuntoPlayer()
    {
        if (isRunning)
        {
            agent.speed = robotRunSpeed;
            agent.SetDestination(playerTransform.position);
        }
    }

    void KillRobot()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {

        }
    }
}

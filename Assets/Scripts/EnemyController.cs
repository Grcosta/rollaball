﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public GameObject player;

	
	// Update is called once per frame
	void Update ()
    {
        transform.LookAt(player.transform.position);
        transform.Translate(transform.forward * 5 * Time.deltaTime);
       
	}
}

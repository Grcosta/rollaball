﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLoader : AsyncLoader
{

    public float splashDuration = 3.0f;
    public int sceneIndexToLoad = 1;

    private static GameLoader _instance;

    protected override void Awake()
    {
        Debug.Log("GameLoader Starting");

        if (_instance != null && _instance != this)
        {
            Debug.LogWarning("Duplicate GameLoader detected");
            Destroy(gameObject);
            return;
        }

        _instance = null;

        DontDestroyOnLoad(gameObject);

        GameObject systemsGo = new GameObject("[Services]");
        systemsGo.tag = "Services";

        Transform systemsParent = systemsGo.transform;
        DontDestroyOnLoad(systemsGo);

        Enqueue(InitializeCoreSystems(systemsParent), 1, null);

        CallOnComplete(OnComplete);
    }

    private IEnumerator InitializeCoreSystems(Transform sysParents)
    {
        //ServiceLocator.Register<>
        yield return new WaitForEndOfFrame();
    }

    private void OnComplete()
    {
        Debug.Log("GameLoader Complete");
        StartCoroutine(LoadInitialScene(sceneIndexToLoad));
    }

    private IEnumerator LoadInitialScene(int index)
    {
        Debug.Log("Loading Scene: " + index);
        yield return new WaitForSeconds(splashDuration);
        yield return  SceneManager.LoadSceneAsync(index);
    }
}

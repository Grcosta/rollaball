using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ServiceLocator
{
    private static readonly Dictionary<System.Type, object>_systems = new Dictionary<System.Type, object>();

    static public T Register<T>(object target)
    {
        if(_systems.ContainsKey(typeof(T)))
        {
            Debug.Log("There is already a tyoe of " + typeof(T) + " that exists");
        }
        else
        {
            Debug.Log("Registering " + typeof(T));
            _systems.Add(typeof(T), target);
        }
        return (T)target;
    }

    static public T Get<T>()
    {
        object ret = null;
        _systems.TryGetValue(typeof(T), out ret);
        if (ret == null)
        {
            Debug.Log("Could Not Find " + typeof(T) + " as a registered system");
        }
        return (T)ret;
    }

    static bool Contains<T>()
    {
        return (_systems.ContainsKey(typeof(T)));
    }
}

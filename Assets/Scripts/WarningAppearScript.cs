﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarningAppearScript : MonoBehaviour {

    public bool warningAppear = false;
    [SerializeField]
    private Texture warningTexture;



    // Use this for initialization
    void Start ()
    {
       
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            warningAppear = true;
            DrawWarning();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            warningAppear = false;
            RemoveWarning();
        }
    }

    void DrawWarning()
    {
        if(warningAppear == true)
        {
            GetComponent<MeshRenderer>().enabled = true;
        }
        
    }

    void RemoveWarning()
    {
        if (warningAppear == false)
        {
            GetComponent<MeshRenderer>().enabled = false;
        }
    }
}

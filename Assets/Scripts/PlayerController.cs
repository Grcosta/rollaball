﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    private Rigidbody rb;
    private int score;

    //Display Score
    [SerializeField]
    private GameObject playerCanvas;
    public Text scoreText;

    //Window Messages
    [SerializeField]
    private Canvas masterCanvas;

    //For the screen text
    public Text winMessage;
    public Text loseMessage;
    public Text warningMessage;

    //Audio Section
    public AudioClip powerUpAudio;
    public AudioClip slowdownAudio;
    public AudioClip gameWinAudio;
    public AudioClip gameLoseAudio;
    public AudioClip chompAudio;

    // Use this for initialization
    void Start()
    {
        //Setting canvas to off
        masterCanvas.GetComponent<Canvas>().enabled = false;
        score = 0;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.AddForce(movement * speed);

        EndGameWinCondition();
    }

    void EndGameWinCondition()
    {
        if (score > 9)   //**DEBUG - Change it back to > 9 after testing**//
        {
            masterCanvas.GetComponent<Canvas>().enabled = true;
            winMessage.text = "you win!";
            Text message = masterCanvas.GetComponent<Text>();
            message = winMessage;

            SoundManager.instance.PlaySFX(gameWinAudio);
            //Pause gameplay
            Time.timeScale = 0.0f;
            return;
        }
    }


    void EndGameLooseCondition()
    {
        masterCanvas.GetComponent<Canvas>().enabled = true;
        loseMessage.text = "GAME OVER";
        Text message = masterCanvas.GetComponent<Text>();
        message = loseMessage;
        //Slow time 
        Time.timeScale = 0.2f;
    }

    void OnTriggerEnter(Collider other)
    {
        //Logic for the pickups
        if (other.gameObject.CompareTag("Pickup"))
        {
            other.gameObject.SetActive(false);
            score++;
            scoreText.text = "score: " + score.ToString();
            SoundManager.instance.PlaySFX(chompAudio);
        }

        //Logic to speed up
        else if (other.gameObject.CompareTag("PowerUp"))
        {
            if (speed < 25) 
                speed += 5;
            Debug.Log(speed.ToString());
            SoundManager.instance.PlaySFX(powerUpAudio);
        }

        //Logic to speed down
        else if (other.gameObject.CompareTag("PowerDown"))
        {
            if (speed > 10)
                speed -= 10;
            Debug.Log(speed.ToString());
            SoundManager.instance.PlaySFX(slowdownAudio);
        }

        //Logic to end game - Colliding the walls
        else if (other.gameObject.CompareTag("Wall"))
        {

        }

        //Logic to enemies - Colliding to end game
        else if (other.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("Game Over");
            EndGameLooseCondition();
            SoundManager.instance.PlaySFX(gameLoseAudio);
        }


        else
            Debug.Log("What was that??");
    }

}
